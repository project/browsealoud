<?php

/**
 * Administrative settings form.
 *                     <script type="text/javascript">var _baLocale = 'au', _baUseCookies = true, _baHiddenMode = false, _baHideOnLoad = true;</script>
 * <script type="text/javascript" src="//www.browsealoud.com/plus/scripts/ba.js"></script>
 */
function browsealoud_admin_settings() {


  $form['browsealoud_locale'] = array(
    '#type' => 'textfield',
    '#title' => t('_baLocale setting'),
    '#default_value' => variable_get('browsealoud_locale', 'us'),
    '#size' => 5,
  );

  $form['browsealoud_cookies'] = array(
    '#type' => 'checkbox',
    '#title' => t('_baUseCookies enabled'),
    '#default_value' => variable_get('browsealoud_cookies', FALSE),
  );


  $form['browsealoud_hiddenmode'] = array(
    '#type' => 'checkbox',
    '#title' => t('_baHiddenMode enabled'),
    '#default_value' => variable_get('browsealoud_hiddenmode', FALSE),
  );

  $form['browsealoud_hideonload'] = array(
    '#type' => 'checkbox',
    '#title' => t('_baHideOnLoad enabled'),
    '#default_value' => variable_get('browsealoud_hideonload', FALSE),
  );

  $form['browsealoud_bamode'] = array(
    '#type' => 'textfield',
    '#title' => t('_baMode setting'),
    '#default_value' => variable_get('browsealoud_bamode', ''),
    '#description' => t('Display a custom image of your own choosing to allow your website visitors to launch the BrowseAloud Plus Toolbar, enter the URL here. (not yet implemented)'),
  );

  $full_form = system_settings_form($form);

  return $full_form;
}
